package com.devcamp.oddevenapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckNumberController {
    @GetMapping("/checknumber")
    public String checkNumber(@RequestParam int num) {
        if (num % 2 == 0) {
            return num + " là số chẵn";
        } else {
            return num + " là số lẻ";
        }
    }
}
